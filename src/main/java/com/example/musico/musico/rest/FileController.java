package com.example.musico.musico.rest;


import com.example.musico.musico.Repositories.ServiceProviderRepository;
import com.example.musico.musico.Repositories.UserRepository;
import com.example.musico.musico.model.ServiceProvider;
import com.example.musico.musico.model.UploadResponse;
import com.example.musico.musico.model.User;
import com.example.musico.musico.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/files")
@CrossOrigin("*")
public class FileController {

    public String fotoserver = "http://www.viljar.ee/muskus/";

    @Autowired
    private FileService fileService;

    @Autowired
    private ServiceProviderRepository serviceProviderRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/upload")
    public UploadResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException, InterruptedException {
        User loggedInUser = userRepository.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        int userID = loggedInUser.getId();
        String fileName = fileService.storeFile(file);
        String fileDownloadUri = fotoserver + fileName;
        UploadResponse uploadResponse = new UploadResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
        userRepository.uploadPicture(uploadResponse, userID);
        return uploadResponse;
    }

    @GetMapping("/file/{serviceproviderid}")
    public String downloadFile(@PathVariable("serviceproviderid") int serviceproviderid) {
        ServiceProvider serviceprovider = serviceProviderRepository.getServiceProvider(serviceproviderid);
        return fotoserver + serviceprovider.getPicture_url();
    }

}
