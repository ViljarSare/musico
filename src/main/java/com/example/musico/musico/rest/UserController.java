package com.example.musico.musico.rest;

import com.captcha.botdetect.web.servlet.CaptchaServlet;
import com.example.musico.musico.Repositories.UserRepository;
import com.example.musico.musico.dto.JwtRequestDto;
import com.example.musico.musico.dto.JwtResponseDto;
import com.example.musico.musico.dto.UpdateUserInformationDTO;
import com.example.musico.musico.dto.UserRegistrationDto;
import com.example.musico.musico.model.ServiceProvider;
import com.example.musico.musico.model.User;
import com.example.musico.musico.model.UserInformation;
import com.example.musico.musico.service.ServiceProviderService;
import com.example.musico.musico.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ServiceProviderService serviceProviderService;

    @Bean
    ServletRegistrationBean captchaServletRegistration () {
        ServletRegistrationBean srb = new ServletRegistrationBean();
        srb.setServlet(new CaptchaServlet());
        srb.addUrlMappings("/botdetectcaptcha");
        return srb;
    }

    @PostMapping("/register")
    public void addUser(@RequestBody UserRegistrationDto userRegistration) {
        userService.register(userRegistration);

    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        JwtResponseDto user = userService.authenticate(request);
        ServiceProvider serviceProvider = serviceProviderService.getServiceProvider(user.getId());
        user.setServiceProviders(Arrays.asList(serviceProvider));
        return user;
    }

    @PostMapping("/fillInformation")
    public void fillInformation(@RequestBody UserInformation information) {
        User loggedInUser = userRepository.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        int userID = loggedInUser.getId();
        userService.addInformation(information, userID);
    }

    @PostMapping("/update")
    public void updateInformation(@RequestBody UpdateUserInformationDTO update){
        User loggedInUser = userRepository.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        int userID = loggedInUser.getId();
        userService.updateInformation(update, userID);

    }




}
