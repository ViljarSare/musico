package com.example.musico.musico.rest;

import com.captcha.botdetect.web.servlet.CaptchaServlet;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

public class Captcha {

    @SpringBootApplication
    public class SpringBootWebApplication {

        @Bean
        ServletRegistrationBean captchaServletRegistration () {
            ServletRegistrationBean srb = new ServletRegistrationBean();
            srb.setServlet(new CaptchaServlet());
            srb.addUrlMappings("/botdetectcaptcha");
            return srb;
        }
    }
}
