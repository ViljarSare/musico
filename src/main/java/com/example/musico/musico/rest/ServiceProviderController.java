package com.example.musico.musico.rest;


import com.example.musico.musico.model.ServiceProvider;
import com.example.musico.musico.model.ServiceProviderInitial;
import com.example.musico.musico.service.ServiceProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/serviceproviders", method = GET)
@CrossOrigin("*")
public class ServiceProviderController {

    @Autowired
    private ServiceProviderService serviceProvider;

    @GetMapping
    public List<ServiceProviderInitial> getServiceProviders() {
        return serviceProvider.getServiceProviderInitial();
    }

    @GetMapping("{id}")
    public ServiceProvider getServiceProvider(@PathVariable("id") int id) {
        return serviceProvider.getServiceProvider(id);
    }

//    @GetMapping("/initial/")
//    public ServiceProvider getServiceProviderInitial; {
//        return serviceProvider.getServiceProviderInitial();
//    }


}
