package com.example.musico.musico.service;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;

@Service
public class FTPConnection {

    //    Defineerime FTP kliendi ja fis objektid
    FTPClient client = new FTPClient();
    FileInputStream fis = null;

    // Defineerimine ühenduse parameetrid
    private String server = "ftp.viidikas.ee";
    private int port = 21;
    private String user = "muskus@viljar.ee";
    public String password = "Muskusveis123456";

    void storeToFTP(String filename, MultipartFile file) throws SocketException, IOException, FileNotFoundException, InterruptedException {

        // Ühendame serveriga
        client.connect(server);
        client.setFileType(FTP.BINARY_FILE_TYPE);
        client.login(user, password);

        //Laeme faili üles ja kui fail on üles laetud, sulgeme ühenduse.
        boolean done = client.storeFile(filename, new BufferedInputStream(file.getInputStream()));
        if (done) {
            Thread.sleep(1000);
            client.logout();
        }
    }

}

