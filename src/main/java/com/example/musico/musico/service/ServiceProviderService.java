package com.example.musico.musico.service;

import com.example.musico.musico.Repositories.ServiceProviderRepository;
import com.example.musico.musico.model.ServiceProvider;
import com.example.musico.musico.model.ServiceProviderInitial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//import io.jsonwebtoken.lang.Assert;

@Service
public class ServiceProviderService {

 @Autowired
private ServiceProviderRepository serviceProviderRepository;
//
//public List<ServiceProvider> getServiceProviders(){
//    return serviceProviderRepository.();
//}

public ServiceProvider getServiceProvider(int id){
    ServiceProvider serviceProvider = serviceProviderRepository.getServiceProvider(id);
  return serviceProvider;
}

    public List<ServiceProviderInitial> getServiceProviderInitial(){
       return serviceProviderRepository.getServiceProviderInitialInfo();

    }



}
