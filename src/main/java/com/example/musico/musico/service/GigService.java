package com.example.musico.musico.service;

import com.example.musico.musico.Repositories.GigRepository;
import com.example.musico.musico.model.Gig;
import com.example.musico.musico.Transform.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class GigService {

    @Autowired
    private GigRepository GigRepository;

    public List<Gig> getGigs() {
        return GigRepository.getGigs();

    }

    public Gig getGig(int id) {
        Gig gig = (Gig) GigRepository.getGig(id);
        return gig;
    }

    public void saveGig(Gig gig){
        if (gig.getId() > 0 ){
            GigRepository.updateGig(gig);
        } else {
                Gig addgig = Transformer.toGigModel(gig); {
                    GigRepository.addGig(gig);
            }
        }
    }

}
