package com.example.musico.musico.dto;

import com.example.musico.musico.model.ServiceProvider;

import java.util.List;

public class JwtResponseDto {
    private final int id;
    private final String username;
    private final String token;
    private final String forename;
    private final String surname;
    private final String email;
    public List<ServiceProvider> serviceProviders;


    public JwtResponseDto(int id, String username, String token, String forename, String surname, String email) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.forename = forename;
        this.surname = surname;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return this.token;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public List<ServiceProvider> getServiceProviders() {
        return serviceProviders;
    }

    public void setServiceProviders(List<ServiceProvider> serviceProviders) {
        this.serviceProviders = serviceProviders;
    }
}
