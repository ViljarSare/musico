package com.example.musico.musico.Repositories;

import com.example.musico.musico.dto.UpdateUserInformationDTO;
import com.example.musico.musico.model.UploadResponse;
import com.example.musico.musico.model.User;
import com.example.musico.musico.model.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean userExists(String username) {

        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},

                Integer.class
        );
        return count != null && count > 0;
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query("SELECT * FROM `user` WHERE `username` = ?", new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("forename"),
                        rs.getString("surname"),
                        rs.getNString("email")));
        return users.size() > 0 ? users.get(0) : null;
    }

//    public JwtResponseDto userdata(String username) {
//        List<User> userdata = jdbcTemplate.query("SELECT * FROM `user` WHERE `username` = ?", new Object[]{username},
//                (rs, rowNum) -> new JwtResponseDto(
//                        rs.getInt("id"),
//                        rs.getString("username"),
//                        rs.getString("password"),
//                        rs.getString("forename"),
//                        rs.getNString("surname")));
//
//        return userdata.size() > 0 ? userdata.get(0) : null;
//    }


    public void addUser(User user) {
        jdbcTemplate.update("INSERT INTO user (username, password, forename, surname, email) VALUES(?, ?, ?, ?, ?)",
                user.getUsername(),
                user.getPassword(),
                user.getForename(),
                user.getSurname(),
                user.getEmail());
    }

    public void addInformation(UserInformation information, int userId) {
        jdbcTemplate.update("INSERT INTO service_provider (user_id, instrument, location, experience, genres, price_range, user_type, picture) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                userId,
                information.getInstrument(),
                information.getLocation(),
                information.getExperience(),
                information.getGenres(),
                information.getPrice_range(),
                information.getUser_type(),
                information.getPicture());
    }

    public void updateInformation(UpdateUserInformationDTO update, int userId) {
        jdbcTemplate.update("UPDATE service_provider, user SET service_provider.instrument = ?, service_provider.location = ?, service_provider.experience = ? , service_provider.genres = ?, service_provider.price_range = ?, service_provider.about = ?, user.forename = ?, user.surname = ?, user.email = ? WHERE user.id=service_provider.user_id AND user.id = ?",
                update.getInstrument(),
                update.getLocation(),
                update.getExperience(),
                update.getGenres(),
                update.getPrice_range(),
                update.getAbout(),
                update.getForename(),
                update.getSurname(),
                update.getEmail(),
                userId);
    }

    public void uploadPicture(UploadResponse uploadResponse, int userId) {
        jdbcTemplate.update("UPDATE service_provider, user SET picture = ? WHERE service_provider.user_id = user.id AND user.id = ?",
                uploadResponse.getName(),
                userId);
    }


}
