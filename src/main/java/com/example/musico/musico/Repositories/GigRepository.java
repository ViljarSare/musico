package com.example.musico.musico.Repositories;

import com.example.musico.musico.model.Gig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GigRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Väljasta kõik Gigid
    public List<Gig> getGigs() {
        return jdbcTemplate.query("select * from gigs", mapGigRows);
    }

    //    Väljast ainult üks Gig
    public List<Gig> getGig(int id) {
        List<Gig> gigs = jdbcTemplate.query("select * from gigs where id = ?", new Object[]{id}, mapGigRows);
        return gigs;
    }

    private RowMapper<Gig> mapGigRows = (rs, rowNum) -> {
        Gig gig = new Gig();
        gig.setId(rs.getInt("id"));
        gig.setDate(rs.getString("date"));
        gig.setDescription(rs.getString("description"));
        gig.setInstrument(rs.getString("needed_instrument"));
        gig.setLocation(rs.getString("location"));
        gig.setPay(rs.getInt("pay"));
        return gig;
    };

    // Lisa gig
    public void addGig(Gig gig) {
        jdbcTemplate.update(
                "INSERT INTO gigs (date, needed_instrument,  description, location, pay) VALUES (?, ?, ?, ?, ?)",
                gig.getDate(),
                gig.getInstrument(),
                gig.getDescription(),
                gig.getLocation(),
                gig.getPay()
        );
    }

    public void updateGig(Gig gig) {

        jdbcTemplate.update("UPDATE gigs SET date = ?, needed_instrument = ?, description = ?, location = ?, pay = ? where id = ?",
                gig.getDate(),
                gig.getInstrument(),
                gig.getDescription(),
                gig.getLocation(),
                gig.getPay(),
                gig.getId()
        );
    }

}
