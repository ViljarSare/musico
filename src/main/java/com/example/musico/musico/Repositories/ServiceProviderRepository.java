package com.example.musico.musico.Repositories;

import com.example.musico.musico.model.ServiceProvider;
import com.example.musico.musico.model.ServiceProviderInitial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceProviderRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    public List<ServiceProvider> getServiceProviders() {
//        return jdbcTemplate.query("SELECT service_provider.*, user.forename, user.surname, user.email FROM service_provider INNER JOIN user ON user.id = service_provider.user_id",
//                mapServiceProviders);
//
//    }

    public ServiceProvider getServiceProvider(int id){
        List <ServiceProvider> provider = jdbcTemplate.query(
                "SELECT service_provider.*, user.forename, user.surname, user.email FROM service_provider INNER JOIN user ON user.id = service_provider.user_id where user_id= ?", new Object[]{id},
                mapServiceProviders);
        return provider.size() > 0 ? provider.get(0) : null;
    }

    private RowMapper<ServiceProvider> mapServiceProviders = (rs, rowNum) -> {
        ServiceProvider serviceprovider = new ServiceProvider();
        serviceprovider.setForename(rs.getString("forename"));
        serviceprovider.setSurname(rs.getNString("surname"));
        serviceprovider.setUser_id(rs.getInt("id"));
        serviceprovider.setInstrument(rs.getString("instrument"));
        serviceprovider.setExperience(rs.getString("experience"));
        serviceprovider.setGenres(rs.getString("genres"));
        serviceprovider.setLocation(rs.getString("location"));
        serviceprovider.setPrice_range(rs.getString("price_range"));
        serviceprovider.setUser_type(rs.getNString("user_type"));
        serviceprovider.setPicture_url(rs.getNString("picture"));
        serviceprovider.setEmail(rs.getNString("email"));
        serviceprovider.setAbout(rs.getNString("about"));
        return serviceprovider;
    };

    public List<ServiceProviderInitial> getServiceProviderInitialInfo(){
        List <ServiceProviderInitial> providerinitial = jdbcTemplate.query("SELECT service_provider.instrument, service_provider.picture, service_provider.experience, service_provider.genres, user.forename, user.id FROM service_provider INNER JOIN user ON user.id = service_provider.user_id", new Object[]{},
                mapServiceProvidersinitial);
        return providerinitial.size() > 0 ? providerinitial : null;
    }

    private RowMapper<ServiceProviderInitial> mapServiceProvidersinitial = (rs, rowNum) -> {
        ServiceProviderInitial serviceproviderInitial = new ServiceProviderInitial();
        serviceproviderInitial.setForename(rs.getString("forename"));
        serviceproviderInitial.setUser_id(rs.getInt("user.id"));
        serviceproviderInitial.setInstrument(rs.getString("instrument"));
        serviceproviderInitial.setExperience(rs.getString("experience"));
        serviceproviderInitial.setGenres(rs.getString("genres"));
        serviceproviderInitial.setPicture_url(rs.getNString("picture"));
        return serviceproviderInitial;
    };


}
