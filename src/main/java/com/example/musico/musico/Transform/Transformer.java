package com.example.musico.musico.Transform;

import com.example.musico.musico.model.Gig;

public class Transformer {

    public static Gig toGigModel(Gig initialobject) {
        if (initialobject ==  null) {
            return null;
        }
        Gig resultGig = new Gig();
        resultGig.setId(initialobject.getId());
        resultGig.setPay(initialobject.getPay());
        resultGig.setLocation(initialobject.getLocation());
        resultGig.setInstrument(initialobject.getInstrument());
        resultGig.setDescription(initialobject.getDescription());
        resultGig.setDate(initialobject.getDate());
        return resultGig;
    }
}
