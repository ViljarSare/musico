package com.example.musico.musico.model;

public class UserRegistration {

    private int id;
    private String username;
    private String password;

    public UserRegistration(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public UserRegistration(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
